package fr.cpe.webService.utils;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cpe.webService.entities.User;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.Date;
import java.util.Map;

@Component
public class JwtUtils {
    @Value("${token.app.jwtSecret}")
    private String jwtSecret;
    @Value("${token.app.jwtExpirationMs}")
    private int jwtExpirationMs;
    public String generateJwtToken(User user) {
        return Jwts.builder()
                .claim("id", user.getId())
                .claim("email", user.getEmail())
                .claim("firstname", user.getFirstname())
                .claim("lastname", user.getLastname())
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    public boolean validateJwtToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch(Exception e ){
           return false;
        }



    }

    public User decodeJwtToken(String authToken) {
        String[] chunks = authToken.split("\\.");
        Base64.Decoder decoder = Base64.getUrlDecoder();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        User user = null;
        try {
            user = mapper.readValue(new String(decoder.decode(chunks[1])), User.class);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return user;
    }
}
