package fr.cpe.webService.services;

import fr.cpe.webService.dtos.CardDTO;
import fr.cpe.webService.entities.Card;
import fr.cpe.webService.repositories.CardRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

@Service("cardService")
public class CardService
{
    @Autowired
    CardRepository cardRepository;

    private final ModelMapper modelMapper = new ModelMapper();
    //getting all card records
    public List<CardDTO> getAllCard()
    {
        List<Card> cards = new ArrayList<Card>();
        cardRepository.findAll().forEach(cards::add);
        return cards.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }
    //getting a specific record
    public CardDTO getCardById(int id)
    {
        return convertToDto(cardRepository.findById(id).get());
    }
    public void saveOrUpdate(CardDTO card)
    {
        cardRepository.save(convertToEntity(card));
    }
    //deleting a specific record
    public void delete(int id)
    {
        cardRepository.deleteById(id);
    }


    private CardDTO convertToDto(Card card) {
        CardDTO cardDTO = modelMapper.map(card, CardDTO.class);
        cardDTO.setName(card.getName());
        cardDTO.setDescription(card.getDescription());
        cardDTO.setAttack(card.getAttack());
        cardDTO.setDefense(card.getDefense());
        cardDTO.setHp(card.getHp());
        cardDTO.setEnergy(card.getEnergy());
        cardDTO.setPicture_url(card.getPicture_url());
        cardDTO.setId(card.getId());
        return cardDTO;
    }

    private Card convertToEntity(CardDTO cardDto) {
        Card Card = modelMapper.map(cardDto, Card.class);
        Card.setName(cardDto.getName());
        Card.setAttack(cardDto.getAttack());
        Card.setDefense(cardDto.getDefense());
        Card.setDescription(cardDto.getDescription());
        Card.setPicture_url(cardDto.getPicture_url());
        Card.setEnergy(cardDto.getEnergy());
        Card.setHp(cardDto.getHp());
        return Card;
    }
}  