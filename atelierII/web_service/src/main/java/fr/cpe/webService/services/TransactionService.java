package fr.cpe.webService.services;

import fr.cpe.webService.dtos.TransactionDTO;
import fr.cpe.webService.dtos.UserResponseDTO;
import fr.cpe.webService.entities.Transaction;
import fr.cpe.webService.entities.User;
import fr.cpe.webService.repositories.CardRepository;
import fr.cpe.webService.repositories.TransactionRepository;
import fr.cpe.webService.repositories.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("transactionService")
public class TransactionService
{
    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    CardRepository cardRepository;

    private final ModelMapper modelMapper = new ModelMapper();

    //getting all transaction records
    public List<TransactionDTO> getAllTransaction()
    {
        List<Transaction> transactions = new ArrayList<>();
        transactionRepository.findAll().forEach(transactions::add);
        return transactions.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }
    //getting a specific record
    public TransactionDTO getTransactionById(int id)
    {
        return convertToDto(transactionRepository.findById(id).get());
    }
    public void saveOrUpdate(TransactionDTO transaction)
    {
        transactionRepository.save(convertToEntity(transaction));
    }

    public void sell(int id){
        Transaction transaction = transactionRepository.findById(id).get();
        if(transaction.isOnSale()){
            transaction.setOnSale(false);
        }else{
            transaction.setOnSale(true);
        }
        transactionRepository.save(transaction);
    }
    public void runTransaction(int id, UserResponseDTO userResponseDTO){
        User buyer = userRepository.findByEmail(userResponseDTO.getEmail());
        Transaction transaction = transactionRepository.findById(id).get();
        User seller = userRepository.findById(transaction.getUsr().getId()).get();
        transaction.setOnSale(false);
        transaction.setUsr(buyer);
        buyer.setSolde(buyer.getSolde() - transaction.getPrice());
        seller.setSolde(seller.getSolde() + transaction.getPrice());
        transaction.setPrice(0);
        userRepository.save(seller);
        userRepository.save(buyer);
        transactionRepository.save(transaction);

    }

    public List<TransactionDTO> getTransactionsForUserId(Integer id){
        Optional<User> user = userRepository.findById(id);
        List<Transaction> transactionList = transactionRepository.findAllByUsr_id(user.get().getId());
        List<TransactionDTO> transactionDTOS = new ArrayList<>();
        for(Transaction transaction:transactionList){
            transactionDTOS.add(convertToDto(transaction));
        }
        return transactionDTOS;
    }

    //deleting a specific record
    public void delete(int id)
    {
        transactionRepository.deleteById(id);
    }
    private TransactionDTO convertToDto(Transaction transaction) {
        TransactionDTO transactionDTO = modelMapper.map(transaction, TransactionDTO.class);
        transactionDTO.setDate(transaction.getDate());
        transactionDTO.setPrice(transaction.getPrice());
        transactionDTO.setOnSale(transaction.isOnSale());
        transactionDTO.setCard(transaction.getCard());
        transactionDTO.setUsr(transaction.getUsr());
        transactionDTO.setId(transaction.getId());
        return transactionDTO;
    }

    private Transaction convertToEntity(TransactionDTO transactionDto) {
        Transaction transaction = modelMapper.map(transactionDto, Transaction.class);
        transaction.setDate(transactionDto.getDate());
        transaction.setPrice(transactionDto.getPrice());
        transaction.setOnSale(transactionDto.isOnSale());
        transaction.setCard(transactionDto.getCard());
        transaction.setUsr(transactionDto.getUsr());
        return transaction;
    }

}  