package fr.cpe.webService.repositories;

import fr.cpe.webService.entities.Transaction;
import fr.cpe.webService.entities.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TransactionRepository extends CrudRepository<Transaction, Integer> {
    List<Transaction> findAllByUsr_id(int usr_id);
}
