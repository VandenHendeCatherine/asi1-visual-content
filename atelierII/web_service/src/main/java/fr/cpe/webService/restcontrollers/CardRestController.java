package fr.cpe.webService.restcontrollers;

import java.util.List;
import java.util.stream.Collectors;

import fr.cpe.webService.dtos.CardDTO;
import fr.cpe.webService.entities.Card;
import fr.cpe.webService.services.CardService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController("cardRestController")
public class CardRestController {
    //autowired the CardService class
    @Autowired
    CardService cardService;

    //creating a get mapping that retrieves all the cards detail from the database
    @GetMapping("/cards")
    private List<CardDTO> getAllCard() {
        List<CardDTO> cards = cardService.getAllCard();
        return cards;
    }

    //creating a get mapping that retrieves the detail of a specific card
    @GetMapping("/cards/{id}")
    private CardDTO getCard(@PathVariable("id") int id) {
        return cardService.getCardById(id);
    }

    //creating a delete mapping that deletes a specific card
    @DeleteMapping("/cards/{id}")
    private void deleteCard(@PathVariable("id") int id) {
        cardService.delete(id);
    }

    //creating Card mapping that Card the card detail in the database
    @PostMapping("/cards")
    private CardDTO saveCard(@RequestBody CardDTO cardDTO) {
        cardService.saveOrUpdate(cardDTO);
        return cardDTO;
    }

}  
