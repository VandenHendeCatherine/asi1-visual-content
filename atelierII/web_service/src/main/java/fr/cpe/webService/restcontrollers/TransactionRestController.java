package fr.cpe.webService.restcontrollers;

import fr.cpe.webService.dtos.TransactionDTO;
import fr.cpe.webService.dtos.UserResponseDTO;
import fr.cpe.webService.services.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("transactionRestController")
public class TransactionRestController {
    //autowired the TransactionService class
    @Autowired
    TransactionService transactionService;



    //creating a get mapping that retrieves all the transactions detail from the database
    @GetMapping("/transactions")
    private List<TransactionDTO> getAllTransaction(@RequestParam(required = false) Integer usr_id) {
        if(usr_id == null){

            return transactionService.getAllTransaction();
        }else {
            return transactionService.getTransactionsForUserId(usr_id);
        }
    }

    //creating a get mapping that retrieves the detail of a specific transaction
    @GetMapping("/transactions/{id}")
    private TransactionDTO getTransaction(@PathVariable("id") int id) {
        return transactionService.getTransactionById(id);
    }

    //creating a delete mapping that deletes a specific transaction
    @DeleteMapping("/transactions/{id}")
    private void deleteTransaction(@PathVariable("id") int id) {
        transactionService.delete(id);
    }

    //creating Transaction mapping that Transaction the transaction detail in the database
    @PostMapping("/transactions")
    private TransactionDTO saveTransaction(@RequestBody TransactionDTO transactionDTO) {
        transactionService.saveOrUpdate(transactionDTO);
        return transactionDTO;
    }

    @PostMapping("/transactions/{id}/sell")
    private int sellTransaction(@PathVariable("id") int id) {
        transactionService.sell(id);
        return id;
    }

    @PostMapping("/transactions/{id}/buy")
    private int buyTransaction(@PathVariable("id") int id, @RequestBody UserResponseDTO targetUser) {
        transactionService.runTransaction(id, targetUser);
        return id;
    }

}  
