package fr.cpe.webService;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication


public class WebServiceApplication {

	public static void main(String[] args) {

		SpringApplication.run(WebServiceApplication.class, args);
	}

}
