package fr.cpe.webService.repositories;

import fr.cpe.webService.entities.Card;
import org.springframework.data.repository.CrudRepository;

public interface CardRepository extends CrudRepository<Card, Integer>
{
}