package fr.cpe.webService.services;

import fr.cpe.webService.dtos.UserResponseDTO;
import fr.cpe.webService.entities.Card;
import fr.cpe.webService.entities.Transaction;
import fr.cpe.webService.entities.User;
import fr.cpe.webService.repositories.CardRepository;
import fr.cpe.webService.repositories.TransactionRepository;
import fr.cpe.webService.repositories.UserRepository;
import fr.cpe.webService.utils.JwtUtils;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

@Service
public class UserService
{
    @Autowired
    UserRepository userRepository;

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    CardRepository cardRepository;
    @Autowired
    JwtUtils jwtUtils;

    private final ModelMapper modelMapper = new ModelMapper();
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    //getting all user records
    public List<UserResponseDTO> getAllUser()
    {
        List<User> users = new ArrayList<User>();
        userRepository.findAll().forEach(users::add);
        return users.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }
    //getting a specific user
    public UserResponseDTO getUserById(int id)
    {
        return convertToDto(userRepository.findById(id).get());
    }
    public void saveOrUpdate(User user)
    {
        if(user.getPassword() != null ){
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            String hashedPassword = passwordEncoder.encode(user.getPassword());
            user.setPassword(hashedPassword);
        }
        userRepository.save(user);
        // give cards to user
        List<Card> cards = (List<Card>) cardRepository.findAll();
        List<Transaction> transactions = new ArrayList<>();
        Random rand = new Random();
        for (int i = 0; i<5; i++){
            Transaction transaction = new Transaction();
            transaction.setOnSale(false);
            transaction.setUsr(user);
            // a modifier
            transaction.setCard(cards.get(rand.nextInt(cards.size())));
            transaction.setDate(new Date());
            transactions.add(transaction);
        }
        transactionRepository.saveAll(transactions);
    }
    //deleting a specific record
    public void delete(int id)
    {
        userRepository.deleteById(id);
    }

    public ResponseEntity<String> login(User user){
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String clearPassword = user.getPassword();
        user = userRepository.findByEmail(user.getEmail());
        if(user != null){
            if(passwordEncoder.matches(clearPassword, user.getPassword())){
                return ResponseEntity.ok()
                        .body(jwtUtils.generateJwtToken(user));
            }else{
                return ResponseEntity.badRequest()
                        .body("Invalid password");
            }
        }else{
            return ResponseEntity.badRequest()
                    .body("User not found");
        }
    }

    private UserResponseDTO convertToDto(User user) {
        UserResponseDTO userDTO = modelMapper.map(user, UserResponseDTO.class);
        userDTO.setEmail(user.getEmail());
        userDTO.setCreated_at(user.getCreated_at());
        userDTO.setFirstname(user.getFirstname());
        userDTO.setId(user.getId());
        userDTO.setLastname(user.getLastname());
        return userDTO;
    }


}  