package fr.cpe.webService.dtos;

import fr.cpe.webService.entities.Card;
import fr.cpe.webService.entities.User;

import javax.persistence.*;
import java.util.Date;

public class TransactionDTO {

    private int id;
    private Date date;
    private boolean OnSale;

    private float price;

    private User usr;

    private Card card;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isOnSale() {
        return OnSale;
    }

    public void setOnSale(boolean onSale) {
        OnSale = onSale;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public User getUsr() {
        return usr;
    }

    public void setUsr(User usr) {
        this.usr = usr;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }
}
