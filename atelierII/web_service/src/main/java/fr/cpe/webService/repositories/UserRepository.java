package fr.cpe.webService.repositories;

import fr.cpe.webService.entities.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer>
{
    User findByEmail(String email);
}