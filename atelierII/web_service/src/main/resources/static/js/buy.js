
const parseToken = parseJwt(localStorage.getItem('token'))
console.log('parse', parseToken)
const GET_CHUCK_URL = "http://localhost:8080/transactions";
let context = {
    method: 'GET'
};
fetch(GET_CHUCK_URL, context)
    .then(response => response.json())
    .then((cardList) => {
        console.log(cardList)
        let template = document.querySelector("#row");
        for (const card of cardList) {
      
            if (card.onSale && card.usr.id != parseToken.id) {
                let clone = document.importNode(template.content, true);
                clone.firstElementChild.id = card.id

                newContent = clone.firstElementChild.innerHTML

                    .replace(/{{family_name}}/g, "family")
                    .replace(/{{img_src}}/g, card.card.picture_url)
                    .replace(/{{name}}/g, card.card.name)
                    .replace(/{{description}}/g, card.card.description)
                    .replace(/{{hp}}/g, card.card.hp)
                    .replace(/{{energy}}/g, card.card.energy)
                    .replace(/{{attack}}/g, card.card.attack)
                    .replace(/{{defense}}/g, card.card.defense)
                    .replace(/{{price}}/g, card.price)
                    .replace(/{{status}}/g, card.onSale ? "onSale" : "Sell")
                   
                    .replace(/{{buyId}}/g, card.id);
                clone.firstElementChild.innerHTML = newContent;

                let cardContainer = document.querySelector("#tableContent");
                cardContainer.appendChild(clone);
            }

        }

        $('tr').click(function () {
            var url = window.location.href;
            if (url.indexOf("?") > 0) {
                url = url.substring(0, url.indexOf("?"));
            }
            url += "?cardId=";
            url += this.id;
            window.location.replace(url);
        });

    })


var searchParams = new URLSearchParams(window.location.search);
let cardPannel = document.querySelector('#card')
// Itère sur les paramètres de recherche.
for (let p of searchParams) {
    console.log(p);
}

console.log("params",)

if (searchParams.get("cardId") != null) {
    const GET_CHUCK_URL = "http://localhost:8080/transactions/" + searchParams.get("cardId");
    let context = {
        method: 'GET'
    };
    fetch(GET_CHUCK_URL, context)
        .then(response => response.json())
        .then((card) => {
            $("#card").load("./part/card-full.html", function () {
                console.log('la card', card)
                let template = document.querySelector("#cardTemplate");

                let clone = document.importNode(template.content, true);
                let newContent = clone.firstElementChild.innerHTML

                    .replace(/{{family_name}}/g, "family")
                    .replace(/{{img_src}}/g, card.card.picture_url)
                    .replace(/{{name}}/g, card.card.name)
                    .replace(/{{description}}/g, card.card.description)
                    .replace(/{{hp}}/g, card.card.hp)
                    .replace(/{{energy}}/g, card.card.energy)
                    .replace(/{{attack}}/g, card.card.attack)
                    .replace(/{{defense}}/g, card.card.defense)
                    .replace(/{{price}}/g, card.price)



                clone.firstElementChild.innerHTML = newContent;

                let cardContainer = document.querySelector("#cardShow");
                cardContainer.appendChild(clone);

            });

        })
}

function buy(e, id){
    e.stopImmediatePropagation()
    fetch("http://localhost:8080/transactions/"+id+"/buy", {
        method: "POST",
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({"email" : parseToken.email })
      })
      .then((res)=>{
          alert('achat effectué')
          window.location = ''
      })
}
