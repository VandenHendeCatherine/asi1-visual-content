var modal1 = document.getElementById('id01');
var modal2 = document.getElementById('id02');

// When the user clicks anywhere outside of the modal1, close it
window.onclick = function(event) {
  if (event.target == modal1) {
    modal1.style.display = "none";
  }
}
// When the user clicks anywhere outside of the modal2, close it
window.onclick = function(event) {
  if (event.target == modal2) {
    modal2.style.display = "none";
  }
}

var submitButtonL = document.querySelector('#login')

function submitFormL(){
  var data = $('#formL').serializeArray().reduce(function (obj, item) {
    obj[item.name] = item.value;
    return obj;
  }, {});
  post('/users/login',data)
  .then((res)=>{
    if(res.status == 200){
      alert("Login");
      res.text().then((result) => {
        console.log(result);
        localStorage.setItem("token", result);
      });
      location.href = "./home.html";
    }else{
      alert("Erreur lors de l'insertion")
    }
  })
}
submitButtonL.addEventListener('click', submitFormL)

var submitButtonR = document.querySelector('#register')

function submitFormR(){
  var data = $('#formR').serializeArray().reduce(function (obj, item) {
    obj[item.name] = item.value;
    return obj;
  }, {});

  post('/users',data)
  .then((res)=>{
    if(res.status == 200){
      alert("Création effectuée");
      document.getElementById('id02').style.display='none';
      document.getElementById('id01').style.display='block';
    }else{
      alert("Erreur lors de l'insertion")
    }
  })
}
submitButtonR.addEventListener('click', submitFormR)