package fr.cpe.asi.authService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cpe.asi.dtos.UserResponseDTO;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Base64;
import java.util.Date;
@Component
public class JwtUtils {
    @Value("${token.app.jwtSecret}")
    private String jwtSecret;
    @Value("${token.app.jwtExpirationMs}")
    private int jwtExpirationMs;

    public String generateJwtToken(UserResponseDTO user) {
        return Jwts.builder()
                .claim("id", user.getId())
                .claim("email", user.getEmail())
                .claim("firstname", user.getFirstname())
                .claim("lastname", user.getLastname())
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    public boolean validateJwtToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch(Exception e ){
            return false;
        }



    }

    public UserResponseDTO decodeJwtToken(String authToken) {
        String[] chunks = authToken.split("\\.");
        Base64.Decoder decoder = Base64.getUrlDecoder();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        UserResponseDTO user = null;
        try {
            user = mapper.readValue(new String(decoder.decode(chunks[1])), UserResponseDTO.class);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return user;
    }

}
