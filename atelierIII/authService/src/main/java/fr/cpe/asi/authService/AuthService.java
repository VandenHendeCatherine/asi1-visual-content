package fr.cpe.asi.authService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import fr.cpe.asi.dtos.UserResponseDTO;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestHeader;
@Service("AuthService")
public class AuthService {
    @Autowired
    JwtUtils jwtUtils;

    public ResponseEntity<String> generateToken(UserResponseDTO user){
        return ResponseEntity.ok()
                .body(jwtUtils.generateJwtToken(user));

    }

    public String validateToken(@RequestHeader("Authorization") String token) {
        return jwtUtils.validateJwtToken(token.split(" ")[1])?"Token valid":"Token invalid";

    }

    public UserResponseDTO decodeToken(@RequestHeader("Authorization") String token) {
        return jwtUtils.decodeJwtToken(token.split(" ")[1]);
    }


}
