package fr.cpe.asi.authService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import fr.cpe.asi.dtos.UserResponseDTO;

@RestController("AuthRestController")
public class AuthRestController {
    @Autowired
    AuthService authService;

    @PostMapping("/auth")
    public ResponseEntity<String> generation(@RequestBody UserResponseDTO user) {

       return authService.generateToken(user);
    }

    @GetMapping("/auth/valid")
    private String validation(@RequestHeader("Authorization") String token) {
        return authService.validateToken(token);
    }

    @GetMapping("/auth/decode")
    private UserResponseDTO decode(@RequestHeader("Authorization") String token) {
        return authService.decodeToken(token);
    }


}
