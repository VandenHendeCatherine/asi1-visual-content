package fr.cpe.asi.fightService;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table
public class Fight {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column
    private Date date;

    @Column
    private int id_deck1;

    @Column
    private int id_deck2;

    @Column
    private int price;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getId_deck1() {
        return id_deck1;
    }

    public void setId_deck1(int id_deck1) {
        this.id_deck1 = id_deck1;
    }

    public int getId_deck2() {
        return id_deck2;
    }

    public void setId_deck2(int id_deck2) {
        this.id_deck2 = id_deck2;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
