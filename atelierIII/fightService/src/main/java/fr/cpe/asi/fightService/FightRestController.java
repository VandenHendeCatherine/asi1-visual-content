package fr.cpe.asi.fightService;


import fr.cpe.asi.dtos.DeckDTO;
import fr.cpe.asi.dtos.FightDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("FightRestController")
public class FightRestController {
    //autowired the fightervice class
    @Autowired
    FightService fightService;



    //creating a get mapping that retrieves all the transactions detail from the database
    @GetMapping("/fights")
    private List<FightDTO> getAllFights(@RequestParam(required = false) Integer usr_id) {
            return fightService.getAllFights();

    }

    //creating a get mapping that retrieves the detail of a specific transaction
    @GetMapping("/fights/{id}")
    private FightDTO getFight(@PathVariable("id") int id) {
        return fightService.getFightById(id);
    }

    //creating a delete mapping that deletes a specific transaction
    @DeleteMapping("/fights/{id}")
    private void deleteFight(@PathVariable("id") int id) {
        fightService.delete(id);
    }

    //creating Deck mapping that Deck the transaction detail in the database
    @PostMapping("/fights")
    private FightDTO saveFight(@RequestBody FightDTO transactionDTO) {
        fightService.saveOrUpdate(transactionDTO);
        return transactionDTO;
    }

    @PostMapping("/fights/{id}/join")
    private FightDTO joinFight(@PathVariable("id") int id, @RequestBody String idDeck){
        return fightService.join(id, Integer.parseInt(idDeck));
    }

    @PostMapping("/fights/{id}/play")
    private String playFight(@PathVariable("id") int id){
        return fightService.play(id);
    }

}  
