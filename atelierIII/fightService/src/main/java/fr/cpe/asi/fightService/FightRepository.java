package fr.cpe.asi.fightService;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FightRepository extends CrudRepository<Fight, Integer> {

}
