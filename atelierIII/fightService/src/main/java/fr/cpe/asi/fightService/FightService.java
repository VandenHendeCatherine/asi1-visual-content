package fr.cpe.asi.fightService;

import com.google.gson.Gson;
import fr.cpe.asi.dtos.CardDTO;
import fr.cpe.asi.dtos.DeckDTO;
import fr.cpe.asi.dtos.FightDTO;
import fr.cpe.asi.dtos.UserResponseDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service("fightService")
public class FightService
{
    @Autowired
    FightRepository fightRepository;

    private final ModelMapper modelMapper = new ModelMapper();

    //getting all transaction records
    public List<FightDTO> getAllFights()
    {
        List<Fight> fights = new ArrayList<>();
        fightRepository.findAll().forEach(fights::add);
        return fights.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }
    //getting a specific record
    public FightDTO getFightById(int id)
    {
        return convertToDto(fightRepository.findById(id).get());
    }
    public void saveOrUpdate(FightDTO transaction)
    {
            fightRepository.save(convertToEntity(transaction));

    }

    //deleting a specific record
    public void delete(int id)
    {
        fightRepository.deleteById(id);
    }
    private FightDTO convertToDto(Fight fight) {
        FightDTO fightDTO = modelMapper.map(fight, FightDTO.class);
        fightDTO.setDate(fight.getDate());
        fightDTO.setPrice(fight.getPrice());
        fightDTO.setId(fight.getId());
        List<DeckDTO> decks = getDecks();
        for(DeckDTO deck : decks){
            if(fight.getId_deck1() == deck.getId()){
                fightDTO.setId_deck1(deck);
            }else if(fight.getId_deck2() == deck.getId()) {
                fightDTO.setId_deck2(deck);
            }
        }
        return fightDTO;
    }

    private Fight convertToEntity(FightDTO fightDTO) {
        Fight fight = modelMapper.map(fightDTO, Fight.class);
        fight.setDate(fightDTO.getDate());
        fight.setPrice(fightDTO.getPrice());
        if(fightDTO.getId_deck1() != null) {
            fight.setId_deck1(fightDTO.getId_deck1().getId());
        }
        if(fightDTO.getId_deck2() != null) {
            fight.setId_deck2(fightDTO.getId_deck2().getId());
        }
        return fight;
    }

    private List<UserResponseDTO> getUsers()
    {
        final String uri = "http://localhost:8010/users";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<UserResponseDTO[]> result = restTemplate.getForEntity(uri, UserResponseDTO[].class);
        if(result.getStatusCode() == HttpStatus.OK){
            return Arrays.asList(Objects.requireNonNull(result.getBody()));
        }
        return null;
    }

    private List<CardDTO> getCards()
    {
        final String uri = "http://localhost:8010/cards";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<CardDTO[]> result = restTemplate.getForEntity(uri, CardDTO[].class);
        if(result.getStatusCode() == HttpStatus.OK){
            return Arrays.asList(Objects.requireNonNull(result.getBody()));
        }
        return null;
    }

    private List<DeckDTO> getDecks()
    {
        final String uri = "http://localhost:8010/decks";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<DeckDTO[]> result = restTemplate.getForEntity(uri, DeckDTO[].class);
        if(result.getStatusCode() == HttpStatus.OK){
            return Arrays.asList(Objects.requireNonNull(result.getBody()));
        }
        return null;
    }
    private void saveUser(UserResponseDTO userResponseDTO){

        final String uri = "http://localhost:8010/users";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForEntity(uri, userResponseDTO,UserResponseDTO.class);
    }

    public FightDTO join(int id, int deckDTO) {
        Fight fight = fightRepository.findById(id).get();
        fight.setId_deck2(deckDTO);
        fightRepository.save(fight);
        return convertToDto(fight);
    }

    public String play(int id) {
       Fight fight = fightRepository.findById(id).get();
       DeckDTO deckDTO1 = null;
       DeckDTO deckDTO2 = null;
       List<DeckDTO> decks = getDecks();
       for(DeckDTO deckDTO : decks){
           if(fight.getId_deck1()==deckDTO.getId()){
               deckDTO1 =deckDTO;
           }else if( fight.getId_deck2() == deckDTO.getId()){
               deckDTO2 = deckDTO;
           }
       }
       if(deckDTO1==null || deckDTO2==null){
           return new Gson().toJson(convertToDto(fight));
       }else{
           List<UserResponseDTO> users = getUsers();
           UserResponseDTO user1 = null;
           UserResponseDTO user2 = null;
           for(UserResponseDTO user : users){
               if(deckDTO1.getUsr_id() == user.getId()){
                   user1 =user;
               }else if( deckDTO2.getUsr_id() == user.getId()){
                   user2 = user;
               }
           }
           float hp2 = deckDTO2.getCardDTO().getDefense() - deckDTO1.getCardDTO().getAttack();
           float hp1 = deckDTO1.getCardDTO().getDefense() - deckDTO2.getCardDTO().getAttack();
           fightRepository.delete(fight);
            if(hp1<hp2){
                user1.setSolde(user1.getSolde()-fight.getPrice());
                user2.setSolde(user2.getSolde()+fight.getPrice());
                saveUser(user1);
                saveUser(user2);
                return "User2";
            }else if (hp1>hp2){
                user1.setSolde(user1.getSolde()+fight.getPrice());
                user2.setSolde(user2.getSolde()-fight.getPrice());
                saveUser(user1);
                saveUser(user2);
               return "User1";
            }else{
                user1.setSolde(user1.getSolde()+fight.getPrice()/2);
                user2.setSolde(user2.getSolde()+fight.getPrice()/2);
                saveUser(user1);
                saveUser(user2);
                return "Ex";
            }

       }
    }
}