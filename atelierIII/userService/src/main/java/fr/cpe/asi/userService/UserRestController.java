package fr.cpe.asi.userService;


import fr.cpe.asi.dtos.UserResponseDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController  
public class UserRestController {

    //autowired the UserService class
    @Autowired
    UserService userService;
    //creating a get mapping that retrieves all the users detail from the database
    @GetMapping("/users")
    private List<UserResponseDTO> getAllUser() {
        List<UserResponseDTO> users = userService.getAllUser();
        return users;
    }

    //creating a get mapping that retrieves the detail of a specific user
    @GetMapping("/users/{id}")
    private UserResponseDTO getUser(@PathVariable("id") int id) {
        return userService.getUserById(id);
    }

    //creating a delete mapping that deletes a specific user
    @DeleteMapping("/users/{id}")
    private void deleteUser(@PathVariable("id") int id) {
        userService.delete(id);
    }

    //creating post mapping that post the user detail in the database
    @PostMapping("/users")
    private int saveUser(@RequestBody User user) {

        userService.saveOrUpdate(user);
        return user.getId();
    }

    @PostMapping("/users/login")
    private ResponseEntity<String> login(@RequestBody User user) {
        return userService.login(user);
    }





   

}
