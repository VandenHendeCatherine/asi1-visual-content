package fr.cpe.asi.userService;

import fr.cpe.asi.dtos.CardDTO;
import fr.cpe.asi.dtos.DeckDTO;
import fr.cpe.asi.dtos.UserResponseDTO;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

@Service
public class UserService
{
    @Autowired
    UserRepository userRepository;

    private final ModelMapper modelMapper = new ModelMapper();
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    //getting all user records
    public List<UserResponseDTO> getAllUser()
    {
        List<User> users = new ArrayList<User>();
        userRepository.findAll().forEach(users::add);
        return users.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }
    //getting a specific user
    public UserResponseDTO getUserById(int id)
    {
        return convertToDto(userRepository.findById(id).get());
    }
    public void saveOrUpdate(User user)
    {
        if(user.getId() == null&& userRepository.findByEmail(user.getEmail()) != null ){
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE,"Utilisateur déjà existant");
        }
        if(user.getPassword() != null ){
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            String hashedPassword = passwordEncoder.encode(user.getPassword());
            user.setPassword(hashedPassword);
        }
        userRepository.save(user);
        // give cards to user
        List<CardDTO> cards = (List<CardDTO>) getCards();
        List<DeckDTO> deckList = new ArrayList<>();
        Random rand = new Random();
        for (int i = 0; i<5; i++){
            DeckDTO deck = new DeckDTO();
            deck.setOnSale(false);
            deck.setUsr_id(user.getId());
            // a modifier
            deck.setCardDTO(cards.get(rand.nextInt(cards.size())));
            deck.setDate(new Date());
            deckList.add(deck);
        }
        saveDecks(deckList);
    }
    //deleting a specific record
    public void delete(int id)
    {
        userRepository.deleteById(id);
    }

    public ResponseEntity<String> login(User user){
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String clearPassword = user.getPassword();
        user = userRepository.findByEmail(user.getEmail());
        if(user != null){
            if(passwordEncoder.matches(clearPassword, user.getPassword())){
                final String uri = "http://localhost:8010/auth";
                RestTemplate restTemplate = new RestTemplate();
                UserResponseDTO userDTO = convertToDto(user);
                ResponseEntity<String> result =  restTemplate.postForEntity(uri, userDTO, String.class);
                if(result.getStatusCode() == HttpStatus.OK){
                    return ResponseEntity.ok()
                            .body(result.getBody());
                }else{
                    return ResponseEntity.badRequest()
                            .body("Impossible de générer le token");
                }


            }else{
                return ResponseEntity.badRequest()
                        .body("Invalid password");
            }
        }else{
            return ResponseEntity.badRequest()
                    .body("User not found");
        }
    }

    private UserResponseDTO convertToDto(User user) {
        UserResponseDTO userDTO = modelMapper.map(user, UserResponseDTO.class);
        userDTO.setEmail(user.getEmail());
        userDTO.setCreated_at(user.getCreated_at());
        userDTO.setFirstname(user.getFirstname());
        userDTO.setId(user.getId());
        userDTO.setLastname(user.getLastname());
        return userDTO;
    }

    private List<CardDTO>  getCards()
    {
        final String uri = "http://localhost:8010/cards";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<CardDTO[]> result = restTemplate.getForEntity(uri, CardDTO[].class);
        if(result.getStatusCode() == HttpStatus.OK){
            return Arrays.asList(Objects.requireNonNull(result.getBody()));
        }
        return null;
    }

    private void saveDecks(List<DeckDTO> deckDTO){
        final String uri = "http://localhost:8010/decks";
        RestTemplate restTemplate = new RestTemplate();
        for(DeckDTO deck: deckDTO){
            restTemplate.postForEntity(uri, deck, DeckDTO.class);
        }
    }

}  