package fr.cpe.asi.cardService;

import org.springframework.data.repository.CrudRepository;

public interface CardRepository extends CrudRepository<Card, Integer>
{
}