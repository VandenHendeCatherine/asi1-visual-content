package fr.cpe.asi.cardService;
import javax.persistence.*;

@Entity
@Table
public class Card {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;
    @Column
    private String name;
    @Column
    private String description;
    @Column
    private String picture_url;
    @Column
    private float energy;
    @Column
    private float hp;
    @Column
    private float defense;
    @Column
    private float attack;
    public Card() {

    }
    public Card(String name, String description, String picture_url, float energy, float hp, float defense, float attack) {
        this.name = name;
        this.description = description;
        this.picture_url = picture_url;
        this.energy = energy;
        this.hp = hp;
        this.defense = defense;
        this.attack = attack;
    }

    @Override
    public String toString() {
        return "{ card :" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", picture_url='" + picture_url + '\'' +
                ", energy=" + energy +
                ", hp=" + hp +
                ", defense=" + defense +
                ", attack=" + attack +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture_url() {
        return picture_url;
    }

    public void setPicture_url(String picture_url) {
        this.picture_url = picture_url;
    }

    public float getEnergy() {
        return energy;
    }

    public void setEnergy(float energy) {
        this.energy = energy;
    }

    public float getHp() {
        return hp;
    }

    public void setHp(float hp) {
        this.hp = hp;
    }

    public float getDefense() {
        return defense;
    }

    public void setDefense(float defense) {
        this.defense = defense;
    }

    public float getAttack() {
        return attack;
    }

    public void setAttack(float attack) {
        this.attack = attack;
    }
}
