### How to use
- Install the requirements (see below)

- Launch the DetectionService.py
    Ignore the few first warning as we do not use a GPU
    /!\ The first launch may take some time as it need to load the weight and import everything

- Do a POST request at http://localhost:5000/tags with an url of an image in a json body, eg :

{
    "url":"https://image.noelshack.com/fichiers/2022/22/3/1654070448-dessinchatcam.jpg"
}

it return as json the list of tags found for the corresponding image (if none or can't open the link will be NULL)

    /!\ Note that the query can take a while to be answered

### Dependencies
- OpenCV
- Tensorflow 2.3.0
- Keras 2.4.0
- Easydict
- Matplotlib

You can simply run:
```bashrc
$ pip install -r requirements.txt
```

### Supported Models
- YOLOv4
