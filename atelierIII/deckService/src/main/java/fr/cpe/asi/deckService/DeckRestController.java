package fr.cpe.asi.deckService;


import fr.cpe.asi.dtos.CardDTO;
import fr.cpe.asi.dtos.DeckDTO;
import fr.cpe.asi.dtos.UserResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController("DeckRestController")
public class DeckRestController {
    //autowired the DeckService class
    @Autowired
    DeckService deckService;



    //creating a get mapping that retrieves all the transactions detail from the database
    @GetMapping("/decks")
    private List<DeckDTO> getAllTransaction(@RequestParam(required = false) Integer usr_id) {
        if(usr_id == null){

            return deckService.getAllTransaction();
        }else {
            return deckService.getTransactionsForUserId(usr_id);
        }
    }

    //creating a get mapping that retrieves the detail of a specific transaction
    @GetMapping("/decks/{id}")
    private DeckDTO getTransaction(@PathVariable("id") int id) {
        return deckService.getTransactionById(id);
    }

    //creating a delete mapping that deletes a specific transaction
    @DeleteMapping("/decks/{id}")
    private void deleteTransaction(@PathVariable("id") int id) {
        deckService.delete(id);
    }

    //creating Deck mapping that Deck the transaction detail in the database
    @PostMapping("/decks")
    private DeckDTO saveTransaction(@RequestBody DeckDTO transactionDTO) {
        deckService.saveOrUpdate(transactionDTO);
        return transactionDTO;
    }

    @PostMapping("/decks/{id}/sell")
    private int sellTransaction(@PathVariable("id") int id) {
        deckService.sell(id);
        return id;
    }

    @PostMapping("/decks/{id}/buy")
    private int buyTransaction(@PathVariable("id") int id, @RequestBody UserResponseDTO targetUser) {
        deckService.runTransaction(id, targetUser);
        return id;
    }

}  
