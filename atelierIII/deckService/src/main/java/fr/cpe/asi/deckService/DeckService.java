package fr.cpe.asi.deckService;

import fr.cpe.asi.dtos.CardDTO;
import fr.cpe.asi.dtos.DeckDTO;
import fr.cpe.asi.dtos.UserResponseDTO;
import org.apache.catalina.connector.Response;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Array;
import java.net.CacheRequest;
import java.util.*;
import java.util.stream.Collectors;

@Service("deckService")
public class DeckService {
    @Autowired
    DeckRepository deckRepository;

    private final ModelMapper modelMapper = new ModelMapper();

    //getting all transaction records
    public List<DeckDTO> getAllTransaction() {
        List<Deck> decks = new ArrayList<>();
        deckRepository.findAll().forEach(decks::add);
        return decks.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    //getting a specific record
    public DeckDTO getTransactionById(int id) {
        return convertToDto(deckRepository.findById(id).get());
    }

    public void saveOrUpdate(DeckDTO transaction) {
        deckRepository.save(convertToEntity(transaction));
    }

    public void sell(int id) {
        Deck deck = deckRepository.findById(id).get();
        if (deck.isOnSale()) {
            deck.setOnSale(false);
        } else {
            deck.setOnSale(true);
        }
        deckRepository.save(deck);
    }

    public void runTransaction(int id, UserResponseDTO userResponseDTO) {
        Deck deck = deckRepository.findById(id).get();
        UserResponseDTO buyer = new UserResponseDTO();
        UserResponseDTO seller = new UserResponseDTO();
        List<UserResponseDTO> userDTOList = getUsers();
        if (userDTOList != null) {
            for (UserResponseDTO userDTO : userDTOList) {
                if (userDTO.getEmail().equals(userResponseDTO.getEmail())) {
                    buyer = userDTO;
                }
                if (userDTO.getId().equals(deck.getUsrId())) {
                    seller = userDTO;
                }
            }
            deck.setOnSale(false);
            deck.setUsrId(buyer.getId());
            buyer.setSolde(buyer.getSolde() - deck.getPrice());
            seller.setSolde(seller.getSolde() + deck.getPrice());
            deck.setPrice(0);
            saveUser(seller);
            saveUser(buyer);
            deckRepository.save(deck);
        }

    }

    public List<DeckDTO> getTransactionsForUserId(Integer id) {
        UserResponseDTO user = new UserResponseDTO();
        List<UserResponseDTO> users = getUsers();
        for (UserResponseDTO userResponseDTO : users) {
            if (userResponseDTO.getId().equals(id)) {
                user = userResponseDTO;
            }
        }
        List<Deck> deckList = deckRepository.findAllByUsrId(user.getId());
        List<DeckDTO> transactionDTOS = new ArrayList<>();
        for (Deck deck : deckList) {
            transactionDTOS.add(convertToDto(deck));
        }
        return transactionDTOS;
    }

    //deleting a specific record
    public void delete(int id) {
        deckRepository.deleteById(id);
    }

    private DeckDTO convertToDto(Deck deck) {
        DeckDTO transactionDTO = modelMapper.map(deck, DeckDTO.class);
        transactionDTO.setDate(deck.getDate());
        transactionDTO.setPrice(deck.getPrice());
        transactionDTO.setOnSale(deck.isOnSale());
        transactionDTO.setUsr_id(deck.getUsrId());
        transactionDTO.setId(deck.getId());

        List<CardDTO> cardDTOS = getCards();
        for (CardDTO cardDTO : cardDTOS) {
            if (deck.getCardId() == cardDTO.getId()) {
                transactionDTO.setCardDTO(cardDTO);
            }
        }
        return transactionDTO;
    }

    private Deck convertToEntity(DeckDTO transactionDto) {
        Deck deck = modelMapper.map(transactionDto, Deck.class);
        deck.setDate(transactionDto.getDate());
        deck.setPrice(transactionDto.getPrice());
        deck.setOnSale(transactionDto.isOnSale());
        deck.setCardId(transactionDto.getCardDTO().getId());
        deck.setUsrId(transactionDto.getUsr_id());
        return deck;
    }

    private List<UserResponseDTO> getUsers() {
        final String uri = "http://localhost:8010/users";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<UserResponseDTO[]> result = restTemplate.getForEntity(uri, UserResponseDTO[].class);
        if (result.getStatusCode() == HttpStatus.OK) {
            return Arrays.asList(Objects.requireNonNull(result.getBody()));
        }
        return null;
    }


    private List<CardDTO> getCards() {
        final String uri = "http://localhost:8010/cards";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<CardDTO[]> result = restTemplate.getForEntity(uri, CardDTO[].class);
        if (result.getStatusCode() == HttpStatus.OK) {
            return Arrays.asList(Objects.requireNonNull(result.getBody()));
        }
        return null;
    }

    private void saveUser(UserResponseDTO userResponseDTO) {

    }
}  