package fr.cpe.asi.deckService;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DeckRepository extends CrudRepository<Deck, Integer> {
    List<Deck> findAllByUsrId(int usrId);
}
