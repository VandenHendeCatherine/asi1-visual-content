package fr.cpe.asi.dtos;

import java.util.Date;

public class FightDTO {

    private int id;

    private Date date;

    private DeckDTO id_deck1;

    private DeckDTO id_deck2;
    private int price;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public DeckDTO getId_deck1() {
        return id_deck1;
    }

    public void setId_deck1(DeckDTO id_deck1) {
        this.id_deck1 = id_deck1;
    }

    public DeckDTO getId_deck2() {
        return id_deck2;
    }

    public void setId_deck2(DeckDTO id_deck2) {
        this.id_deck2 = id_deck2;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
