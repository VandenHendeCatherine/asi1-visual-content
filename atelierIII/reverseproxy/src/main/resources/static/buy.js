const GET_CHUCK_URL = "http://localhost:8010/decks";
let context = {
    method: 'GET'
};
fetch(GET_CHUCK_URL, context)
    .then(response => response.json())
    .then((cardList) => {
        console.log(cardList)
        let template = document.querySelector("#row");
        for (const card of cardList) {
      
            if (card.onSale && card.usr_id != parseToken.id) {
                let clone = document.importNode(template.content, true);
                clone.firstElementChild.id = card.id

                newContent = clone.firstElementChild.innerHTML

                    .replace(/{{family_name}}/g, "family")
                    .replace(/{{img_src}}/g, card.cardDTO.picture_url)
                    .replace(/{{name}}/g, card.cardDTO.name)
                    .replace(/{{description}}/g, card.cardDTO.description)
                    .replace(/{{hp}}/g, card.cardDTO.hp)
                    .replace(/{{energy}}/g, card.cardDTO.energy)
                    .replace(/{{attack}}/g, card.cardDTO.attack)
                    .replace(/{{defense}}/g, card.cardDTO.defense)
                    .replace(/{{price}}/g, card.price)
                    .replace(/{{status}}/g, card.onSale ? "onSale" : "Sell")
                   
                    .replace(/{{buyId}}/g, card.id);
                clone.firstElementChild.innerHTML = newContent;

                let cardContainer = document.querySelector("#tableContent");
                cardContainer.appendChild(clone);
            }

        }

        $('tr').click(function () {
            var url = window.location.href;
            if (url.indexOf("?") > 0) {
                url = url.substring(0, url.indexOf("?"));
            }
            url += "?cardId=";
            url += this.id;
            window.location.replace(url);
        });

    })


var searchParams = new URLSearchParams(window.location.search);
let cardPannel = document.querySelector('#card')
// Itère sur les paramètres de recherche.
for (let p of searchParams) {
    console.log(p);
}

console.log("params",)

if (searchParams.get("cardId") != null) {
    const GET_CHUCK_URL = "http://localhost:8010/decks/" + searchParams.get("cardId");
    let context = {
        method: 'GET'
    };
    fetch(GET_CHUCK_URL, context)
        .then(response => response.json())
        .then((card) => {
            $("#card").load("./card-full.html", function () {
                console.log('la card', card)
                let template = document.querySelector("#cardTemplate");

                let clone = document.importNode(template.content, true);
                let newContent = clone.firstElementChild.innerHTML

                    .replace(/{{family_name}}/g, "family")
                    .replace(/{{img_src}}/g, card.cardDTO.picture_url)
                    .replace(/{{name}}/g, card.cardDTO.name)
                    .replace(/{{description}}/g, card.cardDTO.description)
                    .replace(/{{hp}}/g, card.cardDTO.hp)
                    .replace(/{{energy}}/g, card.cardDTO.energy)
                    .replace(/{{attack}}/g, card.cardDTO.attack)
                    .replace(/{{defense}}/g, card.cardDTO.defense)
                    .replace(/{{price}}/g, card.price)

                clone.firstElementChild.innerHTML = newContent;

                let cardContainer = document.querySelector("#cardShow");
                cardContainer.appendChild(clone);

            });

        })
}

function buy(e, id){
    e.stopImmediatePropagation()
    fetch("http://localhost:8010/decks/"+id+"/buy", {
        method: "POST",
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({"email" : parseToken.email })
      })
      .then((res)=>{
          alert('achat effectué')
          window.location = ''
      })
}
