let yourCards = ""

/*
const GET_decks_URL = "http://localhost:8010/decks";
let contextdeck = {
    method: 'GET'
};
fetch(GET_decks_URL, contextdeck)
    .then(response => response.json())
    .then((cardList) => {
        let template = document.querySelector("#row");
        for (const card of cardList) {
            if (card.onSale && card.usr_id != parseToken.id) {
                yourCards = yourCards + "<button onclick='fight(event, this.parentNode.id, " + card.id + ")'>" + card.name + "</button>"
            }
        }
    })
    */

const GET_CHUCK_URL = "http://localhost:8010/fights";
let context = {
    method: 'GET'
};
fetch(GET_CHUCK_URL, context)
    .then(response => response.json())
    .then((fightList) => {
        console.log(fightList)
        let template = document.querySelector("#row");
        for (const fight of fightList) {
            let clone = document.importNode(template.content, true);
            clone.firstElementChild.id = fight.id

            newContent = clone.firstElementChild.innerHTML
                .replace(/{{price}}/g, fight.price)
                .replace(/{{host}}/g, fight.id_deck1.usr_id)
                .replace(/{{hostCardName}}/g, fight.id_deck1.cardDTO.name)
                .replace(/{{hostCardHp}}/g, fight.id_deck1.cardDTO.hp)
                .replace(/{{fightId}}/g, fight.id)
              
            clone.firstElementChild.innerHTML = newContent;



            let fightContainer = document.querySelector("#tableContent");
            fightContainer.appendChild(clone);

            
        }

        const GET_decks_URL = "http://localhost:8010/decks";
        let contextdeck = {
            method: 'GET'
        };
        fetch(GET_decks_URL, contextdeck)
            .then(response => response.json())
            .then((deckList) => {
                let selectCards = document.querySelectorAll(".selectCard");
                console.log(selectCards)
                selectCards.forEach(select => {
                    for (const deck of deckList) {
                        if (deck.usr_id == parseToken.id) {
                            let opt = document.createElement('option');

                            opt.value = deck.id;
                            opt.innerHTML = deck.cardDTO.name;

                            select.appendChild(opt);
                        }
                    }
                });

            })


    })

var searchParams = new URLSearchParams(window.location.search);
let cardPannel = document.querySelector('#card')
// Itère sur les paramètres de recherche.
for (let p of searchParams) {
    console.log(p);
}

console.log("params",)

if (searchParams.get("cardId") != null) {
    const GET_CHUCK_URL = "http://localhost:8010/decks/" + searchParams.get("cardId");
    let context = {
        method: 'GET'
    };
    fetch(GET_CHUCK_URL, context)
        .then(response => response.json())
        .then((card) => {
            $("#card").load("./card-full.html", function () {
                console.log('la card', card)
                let template = document.querySelector("#cardTemplate");

                let clone = document.importNode(template.content, true);
                let newContent = clone.firstElementChild.innerHTML

                    .replace(/{{family_name}}/g, "family")
                    .replace(/{{img_src}}/g, card.cardDTO.picture_url)
                    .replace(/{{name}}/g, card.cardDTO.name)
                    .replace(/{{description}}/g, card.cardDTO.description)
                    .replace(/{{hp}}/g, card.cardDTO.hp)
                    .replace(/{{energy}}/g, card.cardDTO.energy)
                    .replace(/{{attack}}/g, card.cardDTO.attack)
                    .replace(/{{defense}}/g, card.cardDTO.defense)
                    .replace(/{{price}}/g, card.price)

                clone.firstElementChild.innerHTML = newContent;

                let cardContainer = document.querySelector("#cardShow");
                cardContainer.appendChild(clone);

            });

        })
}

const GET_card_URL = "http://localhost:8010/decks";
let contextcard = {
    method: 'GET'
};
fetch(GET_card_URL, contextcard)
    .then(response => response.json())
    .then((decksList) => {
        console.log(decksList)
        let select = document.querySelector("#listCard");
        for (const deck of decksList) {
            let opt = document.createElement('option');
            if (deck.usr_id == parseToken.id) {
                opt.value = deck.id;
                opt.innerHTML = deck.cardDTO.name;
            }
            select.appendChild(opt);
        }
    })

var modal1 = document.getElementById('id01');
// When the user clicks anywhere outside of the modal1, close it
window.onclick = function (event) {
    if (event.target == modal1) {
        modal1.style.display = "none";
    }
}
var submitButton = document.querySelector('#create')

function submitForm() {
    var data = $('#form').serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});
    post('/fights', { price: data.price, id_deck1: { id: data.id_deck1 } })
        .then((res) => {
            if (res.status == 200) {
                alert("Room created");
                res.text().then((result) => {
                    console.log(result);
                });
                window.location = ''
            } else {
                alert("Erreur lors de la création")
            }
        })
}
submitButton.addEventListener('click', submitForm)

function fight(e, id) {
    e.stopImmediatePropagation()
    console.log("ld" ,id)
    let select = document.querySelector('#select_'+id)
    fetch("http://localhost:8010/fights/" + id+"/join", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ "idDeck": select.value })
    })
        .then((res) => {
            alert("Let's get ready to rumble")
            console.log(res)
        })
}