const parseToken = parseJwt(localStorage.getItem('token'));

const GET_USER_URL = "http://localhost:8010/users/" + parseToken.id;
context = {
    method: 'GET'
};
fetch(GET_USER_URL, context)
    .then(response => response.json())
    .then((user) => {
        $("#user").load("./user.html", function () {
            let template = document.querySelector("#userTemplate");
            let clone = document.importNode(template.content, true);
            let newContent = clone.firstElementChild.innerHTML
                .replace(/{{firstname}}/g, user.firstname)
                .replace(/{{lastname}}/g, user.lastname)
                .replace(/{{solde}}/g, user.solde);
            clone.firstElementChild.innerHTML = newContent;

            let userContainer = document.querySelector("#userShow");
            userContainer.appendChild(clone);
        });
    });