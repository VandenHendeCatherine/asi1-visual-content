const API_URL = "http://localhost:8010";
function get(URL) {
    let context = {
        method: 'GET'
    };

    return fetch(API_URL+URL, context)
        .then(response => response.json())
        
        
}

function post(URL, data){
    return fetch(API_URL+URL, {
    method: "POST",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
}