package fr.cpe.asi.reverseproxy;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class ReverseProxyController {
    @GetMapping(value = { "/{type}/{name}"})
    public String main(
            @PathVariable("type") String type,
            @PathVariable("name") String name
    ){
        type = type.toLowerCase();
        return "forward:/" + type + "/" + name;
    }
}
