var submitButton = document.querySelector('#submit')

function submitForm(){
  var data = $('#form').serializeArray().reduce(function (obj, item) {
    obj[item.name] = item.value;
    return obj;
  }, {});
  data.userId = null

  post(data)
  .then((res)=>{
    if(res.status == 200){
      alert("Ajout correctement effectué")
      location.href = "./home.html";
    }else{
      alert("Erreur lors de l'insertion")
    }
  })
}
submitButton.addEventListener('click', submitForm)

