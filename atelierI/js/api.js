function get() {

    const GET_CHUCK_URL = "https://asi2-backend-market.herokuapp.com/cards";
    let context = {
        method: 'GET'
    };

    return fetch(GET_CHUCK_URL, context)
        .then(response => response.json())
        
        
}

function post(data){
    return fetch("https://asi2-backend-market.herokuapp.com/card", {
    method: "POST",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
}