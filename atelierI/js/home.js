var datas = []
var cardContainer = document.querySelector('#cardContainer')

//cardContainer.style.display = 'none'
cardContainer.style.display = 'block'
$('form input').keydown(function (e) {
    if (e.keyCode == 13) {
        
        e.preventDefault();
        search()
        return false;
    }
});

function init() {
    
    get()
        .then((response) => {
            datas = response
            let datalist = document.querySelector('#datalist')
            response.forEach(element => {
                insertCard(element)
                let option = document.createElement('option')
                option.value = element.name
                datalist.append(option)
            });
        })


}
init()

function deleteCards(){
    let divCard = document.querySelectorAll('.card')
    if(divCard.length>0 ){
        divCard.forEach((c)=>{
            c.remove()
        })
        
    }
}

function insertCard(card) {
   
    let template = document.querySelector("#selectedCard");
    let clone = document.importNode(template.content, true);

    newContent = clone.firstElementChild.innerHTML
        //.replace(/{{family_src}}/g, card.family_src)
        .replace(/{{family_name}}/g, card.family)
        .replace(/{{img_src}}/g, card.imgUrl)
        .replace(/{{name}}/g, card.name)
        .replace(/{{description}}/g, card.description)
        .replace(/{{hp}}/g, card.hp)
        .replace(/{{energy}}/g, card.energy)
        .replace(/{{attack}}/g, card.attack)
        .replace(/{{defense}}/g, card.defence);
    clone.firstElementChild.innerHTML = newContent;
    
    
    //let cardContainer= document.querySelector("#cardContainer");
    cardContainer.appendChild(clone);
}

function search(){
    let searchInput = document.querySelector('#searchInput')
    let index = datas.findIndex(d=>d.name == searchInput.value)
  
    var url = window.location.href;
    if (url.indexOf("?") > 0) {
        url = url.substring(0, url.indexOf("?"));
    }
    if(index != -1){
        deleteCards()
        insertCard(datas[index])
        searchInput.value = ""
    }else{
        alert('Carte inexistante')
    }
}
var searchButton = document.querySelector('#search')

searchButton.addEventListener('click', search)




